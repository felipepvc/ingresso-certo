// GLOBAL > MENU
// Dependencies: Menu_user.destroy()
var Menu = {

	init: function() {

		var pg = $('#page');
		var menu = $('.nav #menu ul');
		var menuContainer = $('.nav .container').first();
		var menu_item = $('.nav #menu ul li');
		var menu_controller = $('.nav #menu-ico');
		var menu_pack = $('.nav #menu ul, .nav #menu ul li, .nav #menu .detail');

		menu_controller.click(function() {

			var ViewportWidth = $(window).width();
			var larguraX = ViewportWidth - 58;

			//Extra small devices (phones, less than 768px)
			if (ViewportWidth < 768) {

				//Close the menu
				if (pg.hasClass('active_menu')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						left: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu');
					menu_item.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart');
					menu.stop(true, false).animate({
						width: "238px",
						left: "-238px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});
					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu	
				else {

					pg.stop(true, false).css('position', 'fixed').animate({
						width: ViewportWidth,
						left: larguraX
					}, 300, 'easeInOutExpo').addClass('active_menu');

					menu_item.css('opacity', '0');

					menu.stop(true, false).css('display', 'block').animate({
						width: larguraX,
						left: '0',
						opacity: '1'
					}, 300, 'easeInOutExpo', function() {
						menu_item.stop(true, false).animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});

					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).stop(true, false).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Small devices (tablets, 768px and up)
			else if (ViewportWidth >= 768 && ViewportWidth <= 991) {

				//Close the menu
				if (pg.hasClass('active_menu')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						left: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu');

					menu_item.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart');
					
					menuContainer.removeAttr("style");

					menu.stop(true, false).animate({
						width: "238px",
						left: "-238px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu	
				else {

					pg.css({
						'position': 'fixed'
					});

					pg.stop(true, false).css('position', 'fixed').animate({
						width: '700px',
						left: larguraX
					}, 300, 'easeInOutExpo', function() {
						$(this).addClass('active_menu')
					});

					menu_item.css('opacity', '0');

					menu.stop(true, false).css('display', 'block').animate({
						width: larguraX,
						left: '0',
						opacity: 1
					}, 300, 'easeInOutExpo', function() {
						menu_item.stop(true, false).animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});
					
					menuContainer.stop(true, false).animate({
						width: "100%"
					}, 150, 'easeInQuart', function(){
						menu_controller.stop(true, false).animate({
							opacity: "0"
						}, 150, 'easeInQuart', function() {
							$(this).addClass('active').animate({
								opacity: "1"
							}, 150, 'easeOutQuart')
						});	
					});

				}

			}

			//Medium and large devices (desktops, 992px and up)
			else {

				//Close the menu
				if (pg.hasClass('active_menu')) {
					menu_pack.stop(true, false).animate({
						opacity: 0,
						top: "-=6px"
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});
					pg.removeClass('active_menu')

				}

				//Open the menu	
				else {

					Menu_user.destroy();

					pg.addClass('active_menu')

					menu_item.css('opacity', '0');

					menu_pack.stop(true, false).css('display', 'block').animate({
						top: "+=6px",
						opacity: 1
					}, 300, 'easeInOutExpo');

				}

			}
		});

		$('body').keydown(function(key) {

			if (key.which == 27) {
				Menu.destroy()
			}

		})

	},

	destroy: function() {

		var pg = $('#page');
		var menu = $('.nav #menu ul');
		var menu_item = $('.nav #menu ul li');
		var menu_controller = $('.nav #menu-ico');
		var menu_pack = $('.nav #menu ul, .nav #menu ul li, .nav #menu .detail');
		var ViewportWidth = $(window).width();

		pg.removeAttr("style");
		menu_controller.removeClass('active');

		if (pg.hasClass('active_menu') && ViewportWidth < 992) {

			pg.stop(true, false).animate({
				width: ViewportWidth,
				left: '0'
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style").removeClass('active_menu');
			})

			menu_item.stop(true, false).animate({
				opacity: "0"
			}, 300, 'easeOutQuart');

			menu.stop(true, false).animate({
				width: "238px",
				left: "-238px",
				opacity: 0
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style").removeClass('active_menu');
			});

		}

		else {

			menu_pack.stop(true, false).animate({
				opacity: 0,
				top: "-=6px"
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style");
			});
			pg.removeClass('active_menu');

		}
	}
}

// GLOBAL > USER MENU
// Dependencies: Menu.destroy()
var Menu_user = {

	init: function() {

		var pg = $('#page');
		var menu = $('.nav #menu-user');
		var menu_item = $('.nav #menu-user li');
		var menu_controller = $('.nav #usermenu-ico');
		var menu_pack = $('.nav #menu-user, .nav #menu-user li, .nav .user-menu-detail');

		menu_controller.click(function() {

			var ViewportWidth = $(window).width();

			//Extra small devices (phones, less than 768px)
			if (ViewportWidth <= 767) {

				//Close the menu
				if (pg.hasClass('active_menu_user')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_user');
					menu.stop(true, false).animate({
						width: "238px",
						right: "-238px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});
					menu_item.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart');
					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').stop(true, false).animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, 'easeInOutExpo').addClass('active_menu_user');

					menu_item.css('opacity', '0');

					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: 1
					}, 300, 'easeInOutExpo', function() {
						menu_item.stop(true, false).animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});

					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Small devices (tablets, 768px and up)
			else if (ViewportWidth >= 768 && ViewportWidth <= 991) {

				//Close the menu
				if (pg.hasClass('active_menu_user')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_user');

					menu.stop(true, false).animate({
						width: "238px",
						right: "-238px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_item.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart');

					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, function() {
						$(this).addClass('active_menu_user')
					});
					menu_item.css('opacity', '0');
					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: '1'
					}, 300, 'easeInOutExpo', function() {
						menu_item.stop(true, false).animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});
					menu_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Medium and large devices (desktops, 992px and up)
			else {

				//Close the menu
				if (pg.hasClass('active_menu_user')) {
					menu_pack.stop(true, false).animate({
						opacity: "0",
						top: "-=6px"
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});
					pg.removeClass('active_menu_user');

				}

				//Open the menu
				else {
					Menu.destroy();
					pg.addClass('active_menu_user');
					menu_pack.stop(true, false).css('display', 'block').animate({
						opacity: 1,
						top: "+=6px"
					}, 300, 'easeInOutExpo');
				}

			}
		});

		$('body').keydown(function(key) {

			if (key.which == 27) {
				Menu_user.destroy()
			}

		})

	},

	destroy: function() {

		var pg = $('#page');
		var menu = $('.nav #menu-user');
		var menu_item = $('.nav #menu-user li');
		var menu_controller = $('.nav #usermenu-ico')
		var menu_pack = $('.nav #menu-user, .nav #menu-user li, .nav .user-menu-detail');
		var ViewportWidth = $(window).width();

		menu_controller.removeClass('active');

		if (pg.hasClass('active_menu_user') && ViewportWidth <= 991) {

			pg.stop(true, false).animate({
				width: ViewportWidth,
				right: '0'
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style").removeClass('active_menu_user');
			})

			menu.stop(true, false).animate({
				width: "238px",
				right: "-238px",
				opacity: 0
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style").removeClass('active_menu_user');
			});

			menu_controller.stop(true, false).animate({
				opacity: "0"
			}, 150, 'easeInQuart', function() {
				$(this).removeClass('active').animate({
					opacity: "1"
				}, 150, 'easeOutQuart')
			});
		}

		else {

			menu_pack.stop(true, false).animate({
				opacity: "0",
				top: "-=6px"
			}, 300, 'easeInOutExpo', function() {
				$(this).removeAttr("style");
			});
			pg.removeClass('active_menu_user');

		}
	}
}

// GLOBAL > MENU LOGIN AND SIGNUP
// Dependencies: Menu.destroy()
var Login_signup = {

	init: function() {

		var pg = $('#page');
		var menu = $('.nav #menu-loginsignup');
		var menu_login = $('.nav #menu-loginsignup #login');
		var menu_signup = $('.nav #menu-loginsignup #signup');
		var menu_login_controller = $('.nav #loginsignup-ico, .not_logged .btn, .searchbar #loginsignup-ico2');
		var menu_signup_controller = $('.not_logged .signupbtn');
		var menu_close_controller = $('.nav #menu-loginsignup .close');
		var tosignup = $('.nav #menu-loginsignup #login .signup a');
		var tologin = $('.nav #menu-loginsignup #signup .login a');
		var terms_txt = $('.nav #menu-loginsignup #signup form #termstxt');
		var terms_btn = $('.nav #menu-loginsignup #signup form #terms');

		menu_login_controller.click(function() {

			var ViewportWidth = $(window).width();

			//Extra small devices (phones, less than 768px)
			if (ViewportWidth <= 767) {

				//Close the menu
				if (pg.hasClass('active_menu_login') || pg.hasClass('active_menu_signup')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_login').removeClass('active_menu_signup');

					menu.stop(true, false).animate({
						width: "400px",
						right: "-400px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_login.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_signup.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').stop(true, false).animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, 'easeInOutExpo').addClass('active_menu_login');

					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: 1
					}, 300, 'easeInOutExpo', function() {
						menu_login.stop(true, false).css('display', 'block').animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Small devices (tablets, 768px and up)
			else if (ViewportWidth >= 768 && ViewportWidth < 992) {

				//Close the menu
				if (pg.hasClass('active_menu_login') || pg.hasClass('active_menu_signup')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_login').removeClass('active_menu_signup');

					menu.stop(true, false).animate({
						width: "400px",
						right: "-400px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_login.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_signup.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, function() {
						$(this).addClass('active_menu_login')
					});
					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: '1'
					}, 300, 'easeInOutExpo', function() {
						menu_login.stop(true, false).css('display', 'block').animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});
					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Medium and large devices (desktops, 992px and up)
			else {

				//Open the menu
				Menu.destroy();
				$('html, body').stop().animate({
						scrollTop: 0
					}, 300, 'easeInQuart');
				pg.addClass('active_menu_login');
				menu.stop(true, false).css('display', 'block').animate({
					opacity: '1'
				}, 300, 'easeInQuart', function() {
					menu_login.stop(true, false).css('display', 'block').animate({
						opacity: "1"
					}, 300, 'easeOutQuart', function() {
						menu_close_controller.stop(true, false).css('display', 'block').animate({
							opacity: '1'
						}, 300, 'easeOutQuart')
					});
				});

			}
		});

		menu_signup_controller.click(function() {

			var ViewportWidth = $(window).width();

			//Extra small devices (phones, less than 768px)
			if (ViewportWidth <= 767) {

				//Close the menu
				if (pg.hasClass('active_menu_login') || pg.hasClass('active_menu_signup')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_login').removeClass('active_menu_signup');

					menu.stop(true, false).animate({
						width: "400px",
						right: "-400px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_login.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_signup.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').stop(true, false).animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, 'easeInOutExpo').addClass('active_menu_signup');

					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: 1
					}, 300, 'easeInOutExpo', function() {
						menu_signup.stop(true, false).css('display', 'block').animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Small devices (tablets, 768px and up)
			else if (ViewportWidth >= 768 && ViewportWidth <= 991) {

				//Close the menu
				if (pg.hasClass('active_menu_login') || pg.hasClass('active_menu_signup')) {

					pg.stop(true, false).css('position', 'relative').animate({
						width: ViewportWidth,
						right: '0'
					}, 300, 'easeInOutExpo').removeClass('active_menu_login').removeClass('active_menu_signup');

					menu.stop(true, false).animate({
						width: "400px",
						right: "-400px",
						opacity: 0
					}, 300, 'easeInOutExpo', function() {
						$(this).removeAttr("style");
					});

					menu_login.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_signup.stop(true, false).animate({
						opacity: "0"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});

					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).removeClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

				//Open the menu
				else {

					pg.stop(true, false).css('position', 'fixed').animate({
						width: ViewportWidth,
						right: ViewportWidth
					}, 300, function() {
						$(this).addClass('active_menu_signup')
					});
					menu.stop(true, false).css('display', 'block').animate({
						width: ViewportWidth,
						right: '0',
						opacity: '1'
					}, 300, 'easeInOutExpo', function() {
						menu_signup.stop(true, false).css('display', 'block').animate({
							opacity: "1"
						}, 300, 'easeOutQuart');
					});
					menu_login_controller.stop(true, false).animate({
						opacity: "0"
					}, 150, 'easeInQuart', function() {
						$(this).addClass('active').animate({
							opacity: "1"
						}, 150, 'easeOutQuart')
					});

				}

			}

			//Medium and large devices (desktops, 992px and up)
			else {

				//Open the menu
				Menu.destroy();
				pg.addClass('active_menu_login');
				menu.stop(true, false).css('display', 'block').animate({
					opacity: '1'
				}, 300, 'easeInQuart', function() {
					menu_signup.stop(true, false).css('display', 'block').animate({
						opacity: "1"
					}, 300, 'easeOutQuart', function() {
						menu_close_controller.stop(true, false).css('display', 'block').animate({
							opacity: '1'
						}, 300, 'easeOutQuart')
					});
				});

			}

		})

		menu_close_controller.click(function() {
			Login_signup.destroy();
		});

		tosignup.click(function() {

			pg.removeClass('active_menu_login').addClass('active_menu_signup');

			menu_login.stop(true, false).animate({
				opacity: 0
			}, 300, 'easeInQuart', function() {
				menu_login.css('display', 'none');
				menu_signup.stop(true, false).css('display', 'block').animate({
					opacity: "1"
				}, 300, 'easeOutQuart');
			});

		});

		tologin.click(function() {

			pg.removeClass('active_menu_signup').addClass('active_menu_login');

			menu_signup.stop(true, false).animate({
				opacity: 0
			}, 300, 'easeInQuart', function() {
				menu_signup.css('display', 'none');
				menu_login.stop(true, false).css('display', 'block').animate({
					opacity: "1"
				}, 300, 'easeOutQuart');
			});

		});
		
		terms_btn.click(function() {
		    
		    if( $(this).hasClass('active') == false ){
		    	terms_txt.stop(true, false).css('display', 'block').animate({
					height: 200,
					opacity: 1
				}, 300, 'easeOutQuart');
				$(this).addClass('active');
		    }
		    else{
		    	terms_txt.stop(true, false).animate({
					height: 0,
					opacity: 0
				}, 300, 'easeInQuart', function() {
				    $(this).removeAttr('style');
				});
				$(this).removeClass('active');
		    }
		    
		});

		$('body').keydown(function(key) {

			if (key.which == 27) {
				Login_signup.destroy();
			}

		});
		
	},

	destroy: function() {

		var pg = $('#page');
		var menu = $('.nav #menu-loginsignup');
		var menu_login = $('.nav #menu-loginsignup #login');
		var menu_signup = $('.nav #menu-loginsignup #signup');
		var menu_login_controller = $('.nav #loginsignup-ico, .not_logged .btn');
		var menu_signup_controller = $('.not_logged .signupbtn');
		var menu_close_controller = $('.nav #menu-loginsignup .close');
		var menu_pack = $('.nav #menu-loginsignup, .nav #menu-loginsignup #login, .nav #menu-loginsignup #signup');
		var ViewportWidth = $(window).width();

		menu_login_controller.removeClass('active');

		if ((pg.hasClass('active_menu_login') || pg.hasClass('active_menu_signup')) && ViewportWidth <= 991) {

			pg.stop(true, false).animate({
				width: ViewportWidth,
				right: '0'
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style").removeClass('active_menu_login').removeClass('active_menu_signup');
			})

			menu_login.stop(true, false).animate({
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});

			menu_signup.stop(true, false).animate({
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});

			menu.stop(true, false).animate({
				width: "400px",
				right: "-400px",
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});

			menu_login_controller.stop(true, true).animate({
				opacity: "0"
			}, 75, 'easeInQuart', function() {
				$(this).removeClass('active').animate({
					opacity: "1"
				}, 75, 'easeOutQuart')
			});
		}

		else {

			pg.removeAttr("style").removeClass('active_menu_login').removeClass('active_menu_signup');
			menu.stop(true, false).animate({
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});
			menu_login.stop(true, false).animate({
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});
			menu_signup.stop(true, false).animate({
				opacity: 0
			}, 150, 'easeInOutExpo', function() {
				$(this).removeAttr("style")
			});
			menu_close_controller.css({
				'display': 'none',
				'opacity': 0
			})

		}
	}
}

// GLOBAL > NAVIGATION FLYOUT
var Nav = {

	init: function() {
		
		var nav = $('.header .nav')
		var navColor = $('.header .nav').attr('data-bgcolor');
		var ViewportWidth = $(window).width();
		var scroll = $(document).scrollTop();

		if( ViewportWidth < 992 ){
			if( scroll < 10 ){
				nav.stop(true, true).animate({
					backgroundColor: 'transparent'
				}, 150, 'easeInOutQuart')
			}
			
			else {
				nav.stop(true, true).animate({
					backgroundColor: navColor
				}, 150, 'easeInOutQuart');
			}			
		}
		
	}

}

// GLOBAL > SEARCH BAR FLYOUT
// Dependencies: Menu_user_flyout.destroy()
var Searchbar_flyout = {
	
	init: function(){
		
		var headerH = $('.header').height()-20;
		var header_flyout = $('.header-flyout');
		var flyoutColor = $('.header-flyout').attr('data-bgcolor');
		var ViewportWidth = $(window).width();

		if( $(document).scrollTop() < headerH  && ViewportWidth >= 992 ){
			
			header_flyout.stop(true, false).animate({
				top: -150
			}, 300, 'easeInQuart', EventCalendar.destroy );
			
			Menu_user_flyout.destroy();
			
		} else {
			
			header_flyout.stop(true, false).animate({
				top: 0
			}, 150, 'easeOutQuart');
			
		}	
		
	}
	
}

// GLOBAL > SEARCH BAR FLYOUT > USER MENU
// Dependencies: Menu.destroy()
var Menu_user_flyout = {

	init: function() {

		var menu_controller = $('.searchbar #usermenu-ico2');

		menu_controller.click(function() {
			
				//Close the menu
				if ( $(this).hasClass('active_menu_user') ) {
					Menu_user_flyout.destroy()
				}
				
				//Open the menu
				else {
					Menu_user_flyout.create()
				}
		});
		
		$('body').keydown(function(key) {
			
			if (key.which == 27) {
				Menu_user_flyout.destroy()
			}
			
		})
		
	},
	
	destroy: function(){
		
		var menu = $('.searchbar #menu-user2');
		var menu_pack = $('.searchbar #menu-user2, .searchbar .user-menu-detail');
		var menu_controller = $('.searchbar #usermenu-ico2');

		menu_pack.stop(true, false).animate({
			opacity: "0",
			top: "-=6px"
		}, 300, 'easeInOutQuart', function() {
			$(this).removeAttr("style");
		});
		
		$( menu_controller ).removeClass('active_menu_user');
		
	},
	
	create: function(){
		
		var menu = $('.searchbar #menu-user2');
		var menu_pack = $('.searchbar #menu-user2, .searchbar .user-menu-detail');
		var menu_controller = $('.searchbar #usermenu-ico2');
		
		Menu.destroy();
		
		$(menu_controller).addClass('active_menu_user');
		
		menu_pack.stop(true, false).css('display', 'block').animate({
			opacity: 1,
			top: "+=6px"
		}, 300, 'easeInOutQuart');
		
	}
}

// GLOBAL > EVENTS CALENDAR
// Dependencies: Menu.destroy(), Menu_user.destroy(), Menu_user_flyout.destroy()
var EventCalendar = {

	init: function() {

		var ViewportWidth = $(window).width();
		var Searchbar = $('.header .searchbar').length;
		
		if( Searchbar == 1 ){
			
			if( ViewportWidth < 992 ){
				
				var calendarTarget = $('.header .searchbar .drptarget');
				var calendarController = $('.header .searchbar .calendar');
				var calendarContainer = $('.header .searchbar .drpcontainer');
				var calendarOverlay = $('.header .searchbar .drpoverlay');
				
		
				calendarTarget.dateRangePicker({
					singleMonth: true,
					showShortcuts: false,
					showTopbar: false,
					startDate: moment(),
					container: calendarContainer,
					customOpenAnimation: function() {
		
						calendarOverlay.stop(true, false).css('display', 'block').animate({
							opacity: 1
						}, 300, 'easeInQuart');
		
						calendarController.addClass('active').css('z-index', '150');
		
						Menu.destroy();
						Menu_user.destroy();
		
						if (ViewportWidth < 768) {
								
							$(this).stop(true, false).css({
								'opacity': 0,
								'left': -13,
								'right': 'inherit',
								'display': 'block'
							}).animate({
								opacity: 1,
								top: "+=6px"
							}, 300, 'easeInQuart');
							
						}
		
						else {
	
							$(this).stop(true, false).css({
								'opacity': 0,
								'left': 'inherit',
								'right': -30,
								'display': 'block'
							}).animate({
								opacity: 1,
								top: "+=6px"
							}, 300, 'easeInQuart');	
							
						}
					},
					customCloseAnimation: function() {
						calendarOverlay.stop(true, false).animate({
							opacity: 0
						}, 300, 'easeOutQuart', function() {
							calendarOverlay.css('display', 'none');
						});
		
						$(this).stop(true, false).animate({
							opacity: 0,
							top: "-=6px"
						}, 300, 'easeOutQuart', function() {
							$(this).css('display', 'none');
						});
		
						calendarController.removeClass('active').css('z-index', 'auto');
					},
					getValue: function() {
						console.log(this.innerHTML);
					},
					setValue: function(s) {
						console.log(s);
					}
				});
		
				calendarController.click(function(evt) {
					
					var headerH = $('.header').height();
		
					evt.stopPropagation();
		
					//Close the date range picker
					if (calendarController.hasClass('active')) {
						calendarTarget.data('dateRangePicker').close();
					}
		
					//Open the date range picker	
					else {
						
						$('html, body').stop().animate({
							scrollTop: headerH-140
						}, 300, 'easeInQuart');
						
						calendarTarget.data('dateRangePicker').open();
					}
		
				})
	
				$('body').keydown(function(key) {
		
					if (key.which == 27) {
						calendarTarget.data('dateRangePicker').close();
					}
		
				})
				
			}
		
		else{
			
			var calendarTarget = $('.header-flyout .searchbar .drptarget');
			var calendarController = $('.header-flyout .searchbar .calendar, .header .searchbar .calendar');
			var calendarController1 = $('.header-flyout .searchbar .calendar');
			var calendarController2 = $('.header .searchbar .calendar');
			var calendarContainer = $('.header-flyout .searchbar .drpcontainer');
			var calendarOverlay = $('.header-flyout .searchbar .drpoverlay');
			
	
			calendarTarget.dateRangePicker({
				singleMonth: true,
				showShortcuts: false,
				showTopbar: false,
				startDate: moment(),
				container: calendarContainer,
				customOpenAnimation: function() {
	
					calendarOverlay.stop(true, false).css('display', 'block').animate({
						opacity: 1
					}, 300, 'easeInQuart');
	
					calendarController1.addClass('active').css('z-index', '160');
	
					Menu.destroy();
					Menu_user.destroy();
					Menu_user_flyout.destroy();

					if ( ViewportWidth < 1200) {
	
						$(this).stop(true, false).css({
							'opacity': 0,
							'left': 'inherit',
							'right': -13,
							'display': 'block'
						}).animate({
							opacity: 1,
							top: "+=6px"
						}, 300, 'easeInQuart');
						
					}
	
					else {
	
						$(this).stop(true, false).css({
							'opacity': 0,
							'left': 'inherit',
							'right': -3,
							'display': 'block'
						}).animate({
							opacity: 1,
							top: "+=6px"
						}, 300, 'easeInQuart');
						
					}
	
				},
				customCloseAnimation: function() {
					calendarOverlay.stop(true, false).animate({
						opacity: 0
					}, 300, 'easeOutQuart', function() {
						calendarOverlay.css('display', 'none');
					});
	
					$(this).stop(true, false).animate({
						opacity: 0,
						top: "-=6px"
					}, 300, 'easeOutQuart', function() {
						$(this).css('display', 'none');
					});
	
					calendarController.removeClass('active').css('z-index', 'auto');
				},
				getValue: function() {
					console.log(this.innerHTML);
				},
				setValue: function(s) {
					console.log(s);
				}
			});
	
			calendarController.click(function(evt) {
				
				var headerH = $('.header').height();
				
				evt.stopPropagation();
				
				//Close the date range picker
				if (calendarController.hasClass('active')) {
					calendarTarget.data('dateRangePicker').close();
				}
	
				//Open the date range picker	
				else {
					
					$('html, body').stop(true, false).animate({
						scrollTop: headerH-17
					}, 300, 'easeInQuart', function() {
						calendarTarget.data('dateRangePicker').open();    
					});
					
				}
			})

			$('body').keydown(function(key) {
	
				if (key.which == 27) {
					calendarTarget.data('dateRangePicker').close();
				}
	
			})
		}			
			
		}

		
	},

	destroy: function() {
		
		var ViewportWidth = $(window).width();
		var calendarController = $('.header-flyout .searchbar .calendar, .header .searchbar .calendar');
		
		if( calendarController.hasClass('active') ){
			
			if( ViewportWidth < 992 ){
				
				var calendarTarget = $('.header .searchbar .drptarget');
				calendarTarget.data('dateRangePicker').close();
				
			}
			else{
				
				var calendarTarget = $('.header-flyout .searchbar .drptarget');
				calendarTarget.data('dateRangePicker').close();
				
			}			
		}
		
	}

}

// GLOBAL > TOOLTIPS
var Tooltips = {
	
	init: function(){
		
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
		
	}
}

// HOME > CAROUSEL
var Carousel = {

	init: function() {

		var slides = $('#home .header .carousel .slides');
		var slide = $('#home .header .carousel .slides .slide');
		var counter = slide.length;
		var pagination_next = '#home .header .carousel .pagination .forward';
		var pagination_prev = '#home .header .carousel .pagination .backward';
		
		if (counter != 1) {

			slides.on('init', function(event, slick){
				var element = slide.first();
				var color2r = element.attr('data-bgr');
				var color2g = element.attr('data-bgg');
				var color2b = element.attr('data-bgb');
				var color2 = new Array([ color2r ], [ color2g ], [ color2b ]);
				var color1 = new Array([ 10 ], [ 90 ], [ 110 ]); // $brand-primary
				
				Carousel.ChangeColor( color1, color2 );
				Carousel.imgContent( element, "show" );
				Carousel.textContent( element, "in" );
				Carousel.pagination( "in" );

				$(window).focus(function() {
			        slides.slick('slickPlay');
			    });
			
			    $(window).blur(function() {
			        slides.slick('slickPause');
			    });
			    
			});
			
			slides.on('beforeChange', function(event, slick, currentSlide, nextSlide){
				var element = slides.find( ".slide:eq(" + currentSlide + ")" );
				var nextElement = $( ".carousel .slides .slide:eq(" + nextSlide + ")" );
				var color2r = nextElement.attr('data-bgr');
				var color2g = nextElement.attr('data-bgg');
				var color2b = nextElement.attr('data-bgb');
				var color2 = new Array([ color2r ], [ color2g ], [ color2b ]);
				var color1r = element.attr('data-bgr');
				var color1g = element.attr('data-bgg');
				var color1b = element.attr('data-bgb');
				var color1 = new Array([ color1r ], [ color1g ], [ color1b ]);
				
				Carousel.imgContent( element, "hide" );
				Carousel.ChangeColor( color1, color2);
				Carousel.textContent( element, "out" );
				Carousel.pagination( "out" );

				$(window).focus(function() {
			        slides.slick('slickPlay');
			    });
			
			    $(window).blur(function() {
			        slides.slick('slickPause');
			    });
				
			});
			
			slides.on('afterChange', function(event, slick, currentSlide){
				var element = slides.find( ".slide:eq(" + currentSlide + ")" );
				Carousel.imgContent( element, "show" );
				Carousel.textContent( element, "in" );
				Carousel.pagination( "in" );
			});

			slides.slick({
				prevArrow: pagination_prev,
				nextArrow: pagination_next,
				easing: 'easeInOutQuart',
				fade: true,
				autoplay: true,
				autoplaySpeed: 5000,
				speed: 600
			});
			
			$('body').keydown(function(key) {

				if (key.which == 37) {
					slides.slick('slickPrev');
				}
				else if (key.which == 39) {
					slides.slick('slickNext');
				}

			});
			
		}

		else {
			var element = slide;
			var color2r = element.attr('data-bgr');
			var color2g = element.attr('data-bgg');
			var color2b = element.attr('data-bgb');
			var color2 = new Array([ color2r ], [ color2g ], [ color2b ]);
			var color1 = new Array([ 10 ], [ 90 ], [ 110 ]); // $brand-primary
			
			Carousel.pagination('out')
			Carousel.ChangeColor( color1, color2);
			Carousel.imgContent( element, "show" );
			Carousel.textContent( element, "in" );
		}

	},

	imgContent: function( element, command ) {
		
		var content = element.attr('data-img');
		var blendMode = element.attr('data-blendMode');
		var container = $('#home .header .imgcontainer');
		var header = $('#home .header');
		var containerSnippet ="<div class='imgcontainer'><div class='img'></div></div>" 
		var counter = $("#home .header .slides .slide").length;
		var slides = $('#home .header .carousel .slides');
		
		if( command == "hide" ){
			container.find('.img').velocity("transition.fadeOut", { duration: 600, easing: "easeInOutQuint" });
		}
		else if( command == "show" ){
			container.remove()
			header.prepend( containerSnippet );
			header.find(".imgcontainer .img").attr('data-src', content);
			header.find(".imgcontainer").css({
				'mix-blend-mode': blendMode,
				'-webkit-blend-mode': blendMode
			});
			header.find(".imgcontainer .img").lazy({
				beforeLoad: function() {
					if (counter != 1){
						slides.slick('slickPause');
					}
				},
				afterLoad: function() {
					if (counter != 1){
						slides.slick('slickPlay');
					}
					header.find(".imgcontainer .img").velocity("transition.shrinkIn", { duration: 5000, easing: "easeInOutQuint" });
				}
			});
		}

		// imgcontainer.stop(true, false).animate({
		// 	opacity: "0"
		// }, 300, 'easeInQuart', function() {
		// 	imgcontainer.remove();
		// 	header.prepend( imgcontainer_snippet );
		// 	$('.header .imgcontainer').attr('data-src', nextbg).css({
		// 		'mix-blend-mode': nextBlendMode,
		// 		'-webkit-blend-mode': nextBlendMode
		// 	});
		// 	$('.header .imgcontainer').lazy({
		// 		effect: "fadeIn",
		// 		effectTime: 300,
		// 		beforeLoad: function() {
		// 			if (counter != 1){
		// 				slides.slick('slickPause');
		// 			}
					
		// 		},
		// 		afterLoad: function() {
		// 			if (counter != 1){
		// 				slides.slick('slickPlay');
		// 			}
		// 			$('.header .imgcontainer').stop(true, true).animate({
		// 				height: '+=8%'
		// 			}, 5000, 'easeOutExpo');
		// 		}
		// 	});
		// });
			
	},
	
	ChangeColor: function( data_bgcolor1, data_bgcolor2 ){
		
		var colors = new Array(
			[ data_bgcolor1[0], data_bgcolor1[1], data_bgcolor1[2] ],
			[ data_bgcolor2[0], data_bgcolor2[1], data_bgcolor2[2] ]
		);
		  
		var gradientOverlay = $('.header .gradientOverlay');
		var nav = $('.header .nav');
		var flyout = $('.header-flyout')
		
		var step = 0;
		var colorIndices = [0,1];
		
		
		//transition speed
		var gradientSpeed = 1/60;
		
		function updateGradient() {
			
			if ( $===undefined ) return;
			
			if( step <= 1 ){
				
				var cTop_0 = colors[colorIndices[0]];
				var cTop_1 = colors[colorIndices[1]];
				var cBottom_0 = colors[colorIndices[0]];
				var cBottom_1 = colors[colorIndices[1]];
				
				var istep = 1 - step;
				var r1 = Math.round(istep * cTop_0[0] + step * cTop_1[0]);
				var g1 = Math.round(istep * cTop_0[1] + step * cTop_1[1]);
				var b1 = Math.round(istep * cTop_0[2] + step * cTop_1[2]);
				var colorTop = "rgba("+r1+","+g1+","+b1+", 0.55)";
				
				var r2 = Math.round(istep * cBottom_0[0] + step * cBottom_1[0]);
				var g2 = Math.round(istep * cBottom_0[1] + step * cBottom_1[1]);
				var b2 = Math.round(istep * cBottom_0[2] + step * cBottom_1[2]);
				var colorBottom = "rgba("+r2+","+g2+","+b2+", 1)";
				
				gradientOverlay.css({
					background: "-webkit-linear-gradient(top, "+colorTop+" 0%,"+colorBottom+" 90%)"
				}).css({
					background: "-moz-linear-gradient(top, "+colorTop+" 0%, "+colorBottom+" 90%)"
				}).css({
					background: "linear-gradient(to bottom, "+colorTop+" 0%,"+colorBottom+" 90%)"
				});
				
				nav.attr("data-bgcolor", colorBottom);
				
				flyout.css('backgroundColor', colorBottom)
				    
				step += gradientSpeed;	
				
			}
		  
		}
		
		function start() {
			setInterval( updateGradient, 10 );
		}
		
		start();
		
	},
	
	textContent: function( element, command ){
		
		var h3 = element.find("h3");
		var h2 = element.find("h2");
		var p = element.find("p");

		
		if ( command == "in" ){
			h3.velocity("transition.slideRightIn", { duration: 600, easing: "easeInOutQuint" });
			h2.velocity("transition.slideRightIn", { duration: 600, delay: 150, easing: "easeInOutQuint" });
			p.velocity("transition.slideRightIn", { duration: 600, delay: 300, easing: "easeInOutQuint" });

		}
		
		else if ( command == "out" ) {
			h3.velocity("stop", true).velocity("transition.slideLeftOut", { duration: 300, delay: 200, easing: "easeInOutQuint" });
			h2.velocity("stop", true).velocity("transition.slideLeftOut", { duration: 300, delay: 100, easing: "easeInOutQuint" });
			p.velocity("stop", true).velocity("transition.slideLeftOut", { duration: 300, easing: "easeInOutQuint" });
		}

	},
	
	pagination: function( command ){
		
		var pagination = $('#home .header .carousel .pagination');
		
		if ( command == "in" ){
			pagination.velocity("transition.fadeIn", { duration: 300, delay: 600, easing: "easeInOutQuart" });
		}
		
		else if ( command == "out" ) {
			pagination.velocity("transition.fadeOut", { duration: 300, easing: "easeInOutQuart" });
		}
	},
	
}

// HOME > CATEGORIES TAB
var Categories_tab = {

	init: function() {

		var tabs_ul = $('.content .categories #categories-tab');
		var tab_controller = $('.content .categories #categories-tab span');
		var tabs = $('.content .categories #categories-tab li');
		var tabs_count = tabs.length;
		var tabs_height = tabs.height();
		var tabs_ul_height = tabs_count * tabs_height + 1;
		var tabs_content = $('.categories .categories-content');
		var ViewportWidth = $(window).width();
		
		if( ViewportWidth >= 768 ){
			
			tabs.css('width', 100/tabs_count + '%');
			
		}

		tab_controller.click(function() {

			if ($(this).hasClass('active')) {

				$(this).stop(true, false).animate({
					opacity: 0
				}, 150, 'easeInQuart', function() {
					$(this).stop(true, false).removeClass('active').animate({
						opacity: 1
					}, 150, 'easeOutQuart');
				});

				tabs_ul.stop(true, false).animate({
					height: tabs_height
				}, 300, 'easeInOutExpo');

			}
			else {

				$(this).stop(true, false).animate({
					opacity: 0
				}, 150, 'easeInQuart', function() {
					$(this).stop(true, false).addClass('active').animate({
						opacity: 1
					}, 150, 'easeOutQuart');
				});

				tabs_ul.stop(true, false).animate({
					height: tabs_ul_height
				}, 300, 'easeInOutExpo');
			}

		});
		
		tabs.click(function(){
			
			if( $(this).hasClass('active') == false ){
				
				var target = $(this).attr('data-target');
				
				tabs.removeClass('active');
				tabs_content.removeClass('active');
				
				$(this).addClass('active');
				
				tabs_content.stop(true, false).animate({
					opacity: 0
				}, 300, 'easeInQuart', function() {
					$('.categories-content#' + target).stop(true, false).addClass('active').animate({
						opacity: 1
					}, 300, 'easeOutQuart');
				});
				
				tabs_ul.stop(true, false).animate({
					height: tabs_height
				}, 300, 'easeInOutExpo');
				
				tab_controller.removeClass('active');
				
				
			}
			
		});

	}

}

// HOME > FEATURED EVENTS
var Featured_events = {
	
	init: function(){
		
		var featured_slides = $('.featured_slides .slide');
		var pagination_next = $('.featured .pagination .forward');
		var pagination_prev = $('.featured .pagination .backward');
			
		featured_slides.slick({
			lazyLoad: 'ondemand',
			prevArrow: pagination_prev,
			nextArrow: pagination_next,
			easing: 'easeInOutQuart',
			mobileFirst: true,
			responsive: [
			    {
			      breakpoint: 300,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2,
			        infinite: true,
			      }
			    },
			    {
			      breakpoint: 768,
			      settings: {
			        slidesToShow: 4,
			        slidesToScroll: 2,
			        infinite: true
			      }
			    }
		    ]
		});
			
	}
	
}

// HOME > PARTNERS CAROUSEL
var Partners_carousel = {
	
	init: function(){
		
		var featured_slides = $('.content .partners ul');
		var pagination_next = $('.content .partners .pagination .forward');
		var pagination_prev = $('.content .partners .pagination .backward');
			
		featured_slides.slick({
			lazyLoad: 'ondemand',
			prevArrow: pagination_prev,
			nextArrow: pagination_next,
			easing: 'easeInOutQuart',
			mobileFirst: true,
			responsive: [
			    {
			      breakpoint: 300,
			      settings: {
			        slidesToShow: 6,
			        slidesToScroll: 3,
			        infinite: true,
			      }
			    },
			    {
			      breakpoint: 767,
			      settings: {
			        slidesToShow: 12,
			        slidesToScroll: 4,
			        infinite: true
			      }
			    }
		    ]
		});
			
	},
	
	BlendEffect: function(){
		
		var slides = $('.content .partners ul div div li img');
		var effect = 'luminosity';
		
		slides.css({
			'mix-blend-mode': effect,
			'-webkit-blend-mode': effect
		});

	}
	
}

// EVENT PAGE > COVER
var Event_cover = {

	LoadBg: function( element ) {
		
		var img = $('.header .imgcontainer .img');
		var event = $('body#event');
		// var header = $('#home .header');
		
		if( event ){
			img.lazy({
				beforeLoad: function() { },
				afterLoad: function() {
					img.velocity("transition.shrinkIn", { duration: 3000, easing: "easeInOutQuint" });
				}
			});
		}
	}
	
}

// EVENT PAGE > PRE-SALE COUNTDOWN
var Event_timer = {
	
	init: function(){
		
		var timer_container = $('.content #tickets.presales #timer')
		var year = timer_container.attr('data-year');
		var month = timer_container.attr('data-month');
		var day = timer_container.attr('data-day');
		var hour = timer_container.attr('data-hour');
		var minute = timer_container.attr('data-minute');
		
		timer_container.countdown({
			until: new Date( year, month-1, day, hour, minute),
			labels: ['Anos', 'Meses', 'Semanas', 'Dias', 'Horas', 'Minutos', 'Segundos'], 
	    	labels1: ['Ano', 'Mês', 'Semana', 'Dia', 'Hora', 'Minuto', 'Segundo'],
	    	padZeroes: true,
	    	format: 'DHMS',
	    	onExpiry: function(){ location.reload() }
		});
		
	}
	
}

// EVENT PAGE > SHOPPING CART
var Event_cart = {
	
	init: function(){
		
		Event_cart.updateAmount();
		Event_cart.addTicket();
		Event_cart.removeTicket();
		
	},
	
	updateAmount: function(){
		
		var display = $('#tickets .amount span');
		var ticket = $('#tickets form .ticket_sku');
		var amount = 0;
		
		ticket.each( function(){
			
			var value = parseFloat( $(this).find('.ticket_value').attr('data-value') ).toFixed(2);
			var qtd = parseInt( $(this).find('.quant input').attr('value') );
			amount += ( value*qtd );
			
		})
		
		display.text( amount.toFixed(2).replace(".",",") );
		
	},
	
	addTicket: function(){
		
		var controller = $('#tickets form .ticket_sku .more');
		
		controller.click( function(){
			
			var qtd = $(this).next('.quant').find('input');
			var qtd_0 = parseInt(qtd.val());
			var qtd_1 = qtd_0+1;

			qtd.attr('value', qtd_1 );
			Event_cart.updateAmount();

		});
		
	},
	
	removeTicket: function(){
		
		var controller = $('#tickets form .ticket_sku .less');
		
		controller.click( function(){
			
			var qtd = $(this).prev('.quant').find('input');
			var qtd_0 = parseInt(qtd.val());
			var qtd_1 = qtd_0-1;

			if(qtd_0 > 0){
				qtd.attr('value', qtd_1 );
				Event_cart.updateAmount();
			}
			
		});
		
	}

	
}

// EVENT PAGE > METAINFO
var Event_meta = {
	
	init: function(){
		
		var controller = $('.content #meta ul li:first-child');
		var controller_ico = $('.content #meta ul li .controller');
		var container = $('.content #meta ul');
		var li = $('.content #meta ul li');
		var li_count = li.length;
		var li_height = li.height()+20;
		var container_height = li_count * li_height;
		
		controller.click( function(){
			
			if ( controller_ico.hasClass('active') ) {

				controller_ico.stop(true, false).animate({
					opacity: 0
				}, 150, 'easeInQuart', function() {
					$(this).stop(true, false).removeClass('active').animate({
						opacity: 1
					}, 150, 'easeOutQuart');
				});

				container.stop(true, false).animate({
					height: li_height
				}, 300, 'easeInOutExpo');

			}
			else {

				controller_ico.stop(true, false).animate({
					opacity: 0
				}, 150, 'easeInQuart', function() {
					$(this).stop(true, false).addClass('active').animate({
						opacity: 1
					}, 150, 'easeOutQuart');
				});

				container.stop(true, false).animate({
					height: container_height
				}, 300, 'easeInOutExpo');
			}
			
			
		})
		
	}
	
}

// EVENT PAGE > REQUIREMENTS
var Event_requirements = {
	
	init: function(){
		
		var controller = $('.content #tickets .requirements .title, .content #tickets .requirements .controller');
		var controller_ico = $('.content #tickets .requirements .controller');
		var container = $('.content #tickets .requirements');
		var ViewportWidth = $(window).width();
		var opened_height = container.height();
		var closed_height = controller.height()+41;		
		container.removeAttr('style');
		
		if( ViewportWidth < 768 ){
			
			container.stop(true, false).animate({
				height: closed_height
			}, 300, 'easeInOutExpo');

			controller.click( function(){
				
				if ( controller_ico.hasClass('active') ) {
	
					controller_ico.stop(true, false).animate({
						opacity: 0
					}, 150, 'easeInQuart', function() {
						$(this).stop(true, false).removeClass('active').animate({
							opacity: 1
						}, 150, 'easeOutQuart');
					});
	
					container.stop(true, false).animate({
						height: closed_height
					}, 300, 'easeInOutExpo');
	
				}
				else {
	
					controller_ico.stop(true, false).animate({
						opacity: 0
					}, 150, 'easeInQuart', function() {
						$(this).stop(true, false).addClass('active').animate({
							opacity: 1
						}, 150, 'easeOutQuart');
					});
	
					container.stop(true, false).animate({
						height: opened_height
					}, 300, 'easeInOutExpo');
				}
				
				
			})
		}
		
		else {
			controller_ico.hide();
		}
		
	}
}

// EVENT PAGE > VIDEOS and INSTAGRAN
var Event_third_contents = {
	
	init: function(){
		
		Event_third_contents.tabs_titles();
		Event_third_contents.video_select();
		Event_third_contents.photo_select();
		
	},
	
	tabs_titles: function(){
		
		var containers = $('.content #gallery .contentContainer');
		var titles = $('.content #gallery .contentTitle');
		var indicators = $('.content #gallery .indicator');
		
		titles.click(function(){
			
			if( $(this).hasClass('active') == false ){
				
				var title_active = $(this);
				var data_target = $(this).attr('data-targetID');
				var target = $('#'+ data_target );
				
				containers.stop(true, false).animate({
					opacity: 0
				}, 300, 'easeInQuart', function(){
					
					containers.removeClass('active');
					
					target.stop(true, false).addClass('active').animate({
						opacity: 1
					}, 300, 'easeOutQuart')
					
					if( data_target == 'instagram' ){
						target.find('.canvas img').lazy({
							effect: "fadeIn",
							effectTime: 300,
							beforeLoad: function() { },
							afterLoad: function() { }
						});
					}
					
					else{
						
					}
				});
				
				indicators.stop(true, false).animate({
					opacity: 0
				}, 300, 'easeInQuart', function(){
					titles.removeClass('active');
					title_active.addClass('active')
					indicators.stop(true, false).animate({
						opacity: 1
					}, 300, 'easeOutQuart')
				});
				
				
			};
			
		});
		
	},
	
	video_select: function(){
		
		var video_canvas = $('.content #gallery #video .canvas');
		var video_thumb = $('.content #gallery #video .thumbs ul li img');
		
		video_thumb.click( function(){
			
			var video_src = $(this).attr('data-videosrc');
			var iframe_snippet = '<iframe src="'+video_src+'" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			
			video_canvas.stop(true, false).animate({
				opacity: 0
			}, 300, 'easeInQuart', function(){
				video_canvas.find('.embed-container').empty();
				video_canvas.find('.embed-container').prepend( iframe_snippet );
				video_canvas.stop(true, false).animate({
					opacity: 1
				}, 300, 'easeOutQuart')
			});
			
		});

	},
	
	photo_select: function(){
		
		var photo_canvas = $('.content #gallery #instagram .canvas');
		var photo_thumb = $('.content #gallery #instagram .thumbs ul li img');
		
		photo_thumb.click( function(){
			
			var photo_src = $(this).attr('data-src');
			var photo_snippet = '<img data-src="'+photo_src+'" width="640px" height="640px"></img>'

			photo_canvas.find('img').stop(true, false).animate({
				opacity: 0
			}, 300, 'easeInQuart', function(){
				photo_canvas.empty()
				photo_canvas.prepend( photo_snippet );
				photo_canvas.find('img').lazy({
					effect: "fadeIn",
					effectTime: 300,
					beforeLoad: function() {
						//photo_canvas.css('opacity', 1);
					},
					afterLoad: function() { }
				});
			});
			
		});
		
	}
	
}

// CART > IDENTIFICATION > SIGNUP TO LOGIN
var Cart_LoginSignup = {

	init: function() {

		var login = $('.content #login');
		var signup = $('.content #signup');
		var tosignup = $('.content #login .signup a');
		var tologin = $('.content #signup .login a');
		var terms_txt = $('.content #signup form #termstxt');
		var terms_btn = $('.content #signup form #terms');
		
		Cart_LoginSignup.ShowContent( login, signup );
		
		tosignup.click(function() {
			
			Cart_LoginSignup.ShowContent( signup, login );

		});

		tologin.click(function() {
			
			Cart_LoginSignup.ShowContent( login, signup );

		});
		
		terms_btn.click(function() {
		    
		    if( $(this).hasClass('active') == false ){
		    	terms_txt.stop(true, false).css('display', 'block').animate({
					height: 200,
					opacity: 1
				}, 300, 'easeOutQuart');
				$(this).addClass('active');
		    }
		    else{
		    	terms_txt.stop(true, false).animate({
					height: 0,
					opacity: 0
				}, 300, 'easeInQuart', function() {
				    $(this).removeAttr('style');
				});
				$(this).removeClass('active');
		    }
		    
		});

	},
	
	ShowContent: function( elementToShow, elementToHide ){
		
		elementToHide.removeClass('active_menu_signup')
		elementToShow.addClass('active_menu_login');

		elementToHide.stop(true, false).animate({
			opacity: 0
		}, 300, 'easeInQuart', function() {
			elementToHide.css('display', 'none');
			elementToShow.stop(true, false).css('display', 'block').animate({
				opacity: "1"
			}, 300, 'easeOutQuart');
		});
		
	}
}

// CART > IDENTIFICATION > SIGNUP TO LOGIN
var Cart_CCtoBoleto = {

	init: function() {

		var A = $('#page #payment #credit');
		var B = $('#page #payment #boleto');
		var toA = $('#page #payment .credit a.btn');
		var toB = $('#page #payment .boleto a.btn');
		var controllers = $('#page #payment .options .btn');

		Cart_CCtoBoleto.ShowContent( A, B, toA, controllers );
		
		toB.click(function() {
			Cart_CCtoBoleto.ShowContent( B, A, $(this), controllers );

		});

		toA.click(function() {
			
			Cart_CCtoBoleto.ShowContent( A, B, $(this), controllers );

		});

	},
	
	ShowContent: function( elementToShow, elementToHide, controllerToActive, allControllers ){
		
		allControllers.removeClass('active')
		controllerToActive.addClass('active');

		elementToHide.stop(true, false).animate({
			opacity: 0
		}, 300, 'easeInQuart', function() {
			elementToHide.css('display', 'none');
			elementToShow.stop(true, false).css('display', 'block').animate({
				opacity: "1"
			}, 300, 'easeOutQuart');
		});
		
	}
	
}

// RESPONSE PAGE > EVENT SUGGEST
var Response_EventSuggest = {
	
	init: function(){
		
		var featured_slides = $('#response_page .content #indications .events');

		featured_slides.slick({
			lazyLoad: 'ondemand',
			easing: 'easeInOutQuart',
			prevArrow: false,
			nextArrow: false,
			dots: true,
			mobileFirst: true,
			responsive: [
			    {
			      breakpoint: 300,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 992,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3,
			        infinite: true
			      }
			    }
		    ]
		});
			
	}
	
}

// MY TICKETS > ORDER DETAILS
var MyTickets_OrderDetail ={
	
	init: function() {

		var controllers = $('#my_tickets .content .ticket_order .main a.btn');
		var meta = $('#my_tickets .content .ticket_order .meta');
		var details = $('#my_tickets .content .ticket_order .details');
		var hr = $('#my_tickets .content .ticket_order hr');
		
		meta.hide();
		details.hide();
		hr.hide();
		
		controllers.click(function(){
			
			var this_meta = $(this).parents('.ticket_order').find('.meta');
			var this_details = $(this).parents('.ticket_order').find('.details');
			var this_hr = $(this).parents('.ticket_order').find('hr');
			
			if( $(this).hasClass('active') ){
				
				$(this).text('+ detalhes');
				$(this).removeClass('active');
				this_meta.slideUp(300);
				this_details.slideUp(300);
				this_hr.slideUp(300);
				
			}
			
			else{
				
				$(this).text('- detalhes');
				$(this).addClass('active');
				this_meta.slideDown(300);
				this_details.slideDown(300);
				this_hr.slideDown(300);
				
			}
			
		})

	}
	
}

// MY TICKETS > EVENT SUGGEST
var MyTickets_EventSuggest = {
	
	init: function(){
		
		var featured_slides = $('#my_tickets .content #indications .events');

		featured_slides.slick({
			lazyLoad: 'ondemand',
			easing: 'easeInOutQuart',
			prevArrow: false,
			nextArrow: false,
			dots: true,
			mobileFirst: true,
			responsive: [
			    {
			      breakpoint: 300,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 1,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 992,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3,
			        infinite: true
			      }
			    }
		    ]
		});
			
	}
	
}

// SEARCH RESULT PAGE > EVENTS LIST
var SearchResult_EventsList = {
	
	init: function(){
		
		var featured_slides = $('#search_result .content #ordinary_list .events');
		var pagination = $('#search_result .content #ordinary_list #pagination .wrapper');

		featured_slides.slick({
			lazyLoad: 'ondemand',
			easing: 'easeInOutQuart',
			prevArrow: false,
			nextArrow: false,
			dots: true,
			appendDots: pagination,
			mobileFirst: true,
			responsive: [
			    {
			      breakpoint: 300,
			      settings: {
			      	rows: 5,
			        slidesPerRow: 1,
			        infinite: true,
			      }
			    },
			    {
			      breakpoint: 767,
			      settings: {
			      	rows: 3,
			        slidesPerRow: 2,
			        infinite: true
			      }
			    },
			    {
			      breakpoint: 992,
			      settings: {
			      	rows: 3,
			        slidesPerRow: 3,
			        infinite: true
			      }
			    }
		    ]
		});
			
	}
	
}

var partner_videos ={
	
	init: function(){
		
		var video_canvas = $('#partner .content #partner_videos .canvas');
		var video_thumb = $('#partner .content #partner_videos .thumbs ul li img');
		
		video_thumb.click( function(){
			
			var video_src = $(this).attr('data-videosrc');
			var iframe_snippet = '<iframe src="'+video_src+'" width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
			
			video_canvas.stop(true, false).animate({
				opacity: 0
			}, 300, 'easeInQuart', function(){
				video_canvas.find('.embed-container').empty();
				video_canvas.find('.embed-container').prepend( iframe_snippet );
				video_canvas.stop(true, false).animate({
					opacity: 1
				}, 300, 'easeOutQuart')
			});
			
		});

	}
	
}

//When the page is loaded
$(document).ready(function() {

	Menu.init();
	Menu_user.init();
	Login_signup.init();
	Nav.init();
	Searchbar_flyout.init();
	Menu_user_flyout.init();
	EventCalendar.init();
	Tooltips.init();
	Carousel.init();
	Categories_tab.init();
	Featured_events.init();
	Partners_carousel.init();
	Event_cover.LoadBg();
	Event_timer.init();
	Event_cart.init();
	Event_meta.init();
	Event_requirements.init();
	Event_third_contents.init();
	Cart_LoginSignup.init();
	Cart_CCtoBoleto.init();
	Response_EventSuggest.init();
	MyTickets_OrderDetail.init();
	MyTickets_EventSuggest.init();
	SearchResult_EventsList.init();
	partner_videos.init();

	//When scrolling the page
	$(window).bind('scroll', function(e) {
		Nav.init();
		Searchbar_flyout.init();
	});

	//When changing the window size
	$(window).resize(function() {
		Menu.destroy();
		Menu_user.destroy();
		Login_signup.destroy();
		EventCalendar.destroy();
	});

	//When changing the device orientation
	$(window).on("orientationchange", function() {
		Menu.destroy();
		Menu_user.destroy();
		Login_signup.destroy();
		EventCalendar.destroy();
		Event_requirements.init();
	});

});