# Table of contents:

## Home

- [Not logged users](http://ingressocerto.ddns.net/repo/ic/home.html)
- [Logged users](http://ingressocerto.ddns.net/repo/ic/home-logged.html)

## Events

- [Another Dimension](http://ingressocerto.ddns.net/repo/ic/event-01-PreSale.html) / Pre sales
- [Carrapetas](http://ingressocerto.ddns.net/repo/ic/event-02-On.html) / Sales on
- [DJ Craze](http://ingressocerto.ddns.net/repo/ic/event-03-Off.html) / Sales off
- [Baile do Zeh Pretim](http://ingressocerto.ddns.net/repo/ic/event-04.html)
- [7th Mardi Gras](http://ingressocerto.ddns.net/repo/ic/event-05.html)

## Shopping cart

- [Identification](http://ingressocerto.ddns.net/repo/ic/cart_01.html)
- [Confirmation](http://ingressocerto.ddns.net/repo/ic/cart_02.html)
- [Tickets](http://ingressocerto.ddns.net/repo/ic/cart_03.html)
- [Payment](http://ingressocerto.ddns.net/repo/ic/cart_04.html)
- [Success](http://ingressocerto.ddns.net/repo/ic/response_01.html)
- [Error](http://ingressocerto.ddns.net/repo/ic/response_02.html)

## User profile

- [My tickets](http://ingressocerto.ddns.net/repo/ic/my_tickets.html)
- [Credit Cards](http://ingressocerto.ddns.net/repo/ic/cards.html)
- [Profile](http://ingressocerto.ddns.net/repo/ic/user_profile.html)

## Others

- [Search Result](http://ingressocerto.ddns.net/repo/ic/search_result.html)

## Partners

- [Le Club](http://ingressocerto.ddns.net/repo/ic/partner_A.html) / Template A
- [360 Sports](http://ingressocerto.ddns.net/repo/ic/partner_B.html) / Template B
- [360 Sports](http://ingressocerto.ddns.net/repo/ic/partner_C.html) / Template C

## Editorial

- [Reveillon](http://ingressocerto.ddns.net/repo/ic/editorial.html)